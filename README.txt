
FeedAPI Data
------------

This module is a FeedAPI processor plugin and uses Data module for storing feed items.

By using Data module, it allocates storage space dynamically depending on the mapping 
information of a feed. Data module also provides dynamic generation of views 
integration.

Once FeedAPI Data is installed, a typical workflow would be:

- Create feed
- Adjust feed's mapping
- Refresh
- Build view with aggregated data

INSTALLATION AND SETUP
----------------------

1) Install FeedAPI data module and all required modules.

* FeedAPI http://drupal.org/project/feedapi
* at least one FeedAPIparser, e. g. Parser Common Syndication
* FeedAPI Mapper 2 http://drupal.org/project/feedapi_mapper
* Data module http://drupal.org/project/data

2) Go to the feed content type that you would like to configure it for. Edit the 
content type and enable FeedAPI Data as processor.

3) Click on the 'map' tab on the content type edit form and configure the mappings 
for your feed.
